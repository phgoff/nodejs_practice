const http = require('http')

//Destructuring 
const { reqlistener } = require('./routes')
const server = http.createServer(reqlistener)

// Or normal
// const routes = require('/.routes')
// const server = http.createServer(routes.reqlistener)


//default port = nodejs 3000 , python 5000
server.listen(3000, () => `Server Started`)

