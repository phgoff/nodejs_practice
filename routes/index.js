const fs = require('fs')

const reqlistener = (req, res) => {
    if(req.url === '/about'){
        fs.writeFile('./test.txt', 'Test Data', () =>{
            res.setHeader('Content-Type', 'text/html')
            res.write('<h1> This is about page')
            return res.end()
        })
   
    }else{
        res.setHeader('Content-Type', 'text/html')
        res.write('<h1> Hello world <br> Nodemon')
        res.end()
    }
}

exports.reqlistener = reqlistener